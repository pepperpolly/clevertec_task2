package com.example.clevertec_task2.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.doOnPreDraw
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.clevertec_task2.common.ItemClickListener
import com.example.clevertec_task2.databinding.MainFragmentBinding
import com.example.clevertec_task2.getRecyclerList
import com.example.clevertec_task2.recycler.MainRecyclerAdapter

class MainFragment : Fragment(), ItemClickListener {

    private lateinit var binding: MainFragmentBinding
    private val myAdapter = MainRecyclerAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val list = getRecyclerList()
        binding.recycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = myAdapter
        }
        myAdapter.submitList(list)
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
    }

    override fun openDetailFragment(title: String, description: String, view: ImageView) {
        val action = MainFragmentDirections.actionMainFragmentToDetailFragment()
        action.title = title
        action.description = description
        val extras = FragmentNavigatorExtras(view to title)
        findNavController().navigate(action, extras)
    }
}