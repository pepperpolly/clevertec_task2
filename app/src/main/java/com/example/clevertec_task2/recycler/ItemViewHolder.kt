package com.example.clevertec_task2.recycler

import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.clevertec_task2.common.ItemClickListener
import com.example.clevertec_task2.databinding.RecyclerItemBinding

class ItemViewHolder(
    private val binding: RecyclerItemBinding,
    private val listener: ItemClickListener
) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Item) {

        binding.apply {
            title.text = item.title
            description.text = item.description
        }
        ViewCompat.setTransitionName(binding.image, item.title)
        binding.root.setOnClickListener {
            listener.openDetailFragment(
                binding.title.text.toString(),
                binding.description.text.toString(),
                binding.image
            )
        }
    }
}