package com.example.clevertec_task2.recycler

data class Item(var title: String, var description: String)