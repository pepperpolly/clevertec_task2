package com.example.clevertec_task2.common

import android.widget.ImageView

interface ItemClickListener {
    fun openDetailFragment(title: String, description: String, view: ImageView)
}