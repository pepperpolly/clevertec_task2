package com.example.clevertec_task2

import com.example.clevertec_task2.recycler.Item


fun getRecyclerList(): MutableList<Item> {
    val list = mutableListOf<Item>()
    for (i in 1..1000) {
        list.add(Item("Title $i", "Description $i"))
    }
    return list
}